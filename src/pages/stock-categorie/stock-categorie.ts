import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StockProduitsPage } from '../stock-produits/stock-produits';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the StockCategoriePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stock-categorie',
  templateUrl: 'stock-categorie.html',
})
export class StockCategoriePage {
  buttons : any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public global:GlobalProvider) {

    this.buttons = [
      {title: 'Produits blancs',id:'Produits blancs'},
      {title: 'Lubrifiants',id:'Lubrifiants'},
      {title: 'Gaz',id:'Gaz'},
      {title: 'Accessoires',id:'Accessoire'}
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StockCategoriePage');
  }

  goToPage(categorie){
    this.navCtrl.push(StockProduitsPage,{"categorie":categorie});
  }
  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

}
