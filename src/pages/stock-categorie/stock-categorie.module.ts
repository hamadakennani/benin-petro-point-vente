import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StockCategoriePage } from './stock-categorie';

@NgModule({
  declarations: [
    StockCategoriePage,
  ],
  imports: [
    IonicPageModule.forChild(StockCategoriePage),
  ],
})
export class StockCategoriePageModule {}
