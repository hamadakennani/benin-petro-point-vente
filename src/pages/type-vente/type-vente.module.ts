import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeVentePage } from './type-vente';

@NgModule({
  declarations: [
    TypeVentePage,
  ],
  imports: [
    IonicPageModule.forChild(TypeVentePage),
  ],
})
export class TypeVentePageModule {}
