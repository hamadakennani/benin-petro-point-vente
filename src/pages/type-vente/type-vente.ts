import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { VentePage } from '../vente/vente';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TypeTvPage } from '../type-tv/type-tv';

/**
 * Generated class for the TypeVentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type-vente',
  templateUrl: 'type-vente.html',
})
export class TypeVentePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,public http: Http,public loading: LoadingController,private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TypeVentePage');
  }


  goToPage(type){
    if(type == "Vente au comptant" || type == "Vente par TV" || type == "Vente par MONO PAY"){
      if(type == "Vente par TV" ){
        this.navCtrl.push(TypeTvPage);
        //this.ScanBarreCodeTV();
      }else{
        this.navCtrl.push(VentePage);
      }
      this.global.type_vente = type;
      this.global.client = "";
      this.global.consomateur = "";
      this.global.MontantCarte = "";
      this.global.carte_id = "";
      this.global.qrcode = "";
    }else{
      this.ScanBarreCode();
      this.global.type_vente = type;
    }
   
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  ScanBarreCodeTV(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.barcodeScanner.scan().then(barcodeData => {
      
      if(barcodeData.text != ""){
        this.http.get(this.global.url+'/existTicket.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          loading.dismissAll();
          if(res == 0){
              this.navCtrl.push(VentePage);
              this.global.qrcode = barcodeData.text;
              this.global.only_read = false;
              //this.global.presentAlert("message","Ticket inexistante","ioio",TypeTvPage,this);
          }else{
            this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client = res[0].client;
                this.global.MontantCarte = res[0].montant;
                this.global.carte_id = res[0].id;
                this.global.qrcode = res[0].qrcode;
                if(res[0].etat == 'util'){
                  this.global.presentAlert("Attention","Ce ticket a été dèjà utilisé dans le passé","ioio",TypeTvPage,this);
                }else{
                  this.navCtrl.push(VentePage);
                  this.global.only_read = true;

                }
                
                
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
      }else{
        loading.dismissAll();
      }
    
      
      }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
      });

}

  ScanBarreCode(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.barcodeScanner.scan().then(barcodeData => {
      //var barcodeData = "94d112b93483fd213a13812a87e609cb"
      if(barcodeData.text != ""){
        this.http.get(this.global.url+'/checkCartByQrcode.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          loading.dismissAll();
          if(res == 0){
              this.global.presentAlert("message","Carte inexistante","ioio",VentePage,this);
          }else{
            this.http.get(this.global.url+'/getInfoParCarte.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client = res.client;
                this.global.consomateur = res.consomateur;
                this.global.MontantCarte = res.solde;
                this.global.carte_id = res.id;
                this.global.qrcode = res.qrcode;
                if(res.state == "terminee"){
                    this.global.presentAlert("Carte Perdu","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state == "expiree"){
                  this.global.presentAlert("Carte Expiree","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state ==  "suspendu"){
                  this.global.presentAlert("Carte Bloquée","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state == "generee" || res.state == "activee"){
                  this.navCtrl.push(VentePage);
                }
                
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
      }else{
        loading.dismissAll();
      }
    
      
      }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
      });

}

}
