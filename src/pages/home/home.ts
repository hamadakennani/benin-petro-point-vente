import { Component, Injectable } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { VentePage } from '../vente/vente';
import { ConsultationPage } from '../consultation/consultation';
import { GlobalProvider } from '../../providers/global/global';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LoadingController } from 'ionic-angular';
import { TypeVentePage } from '../type-vente/type-vente';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
@Injectable()
export class HomePage {

  
  buttons : Array<{title:string,component: any}>;

  constructor(private barcodeScanner: BarcodeScanner,public navCtrl: NavController,public http: Http,public global:GlobalProvider,public alertCtrl:AlertController,public loading: LoadingController) {
    this.buttons = [
     // {title: 'Ventes',component:VentePage},
      {title: 'Consultation',component:ConsultationPage},
    ]
  }

  
  ionViewDidLoad() {
    this.http.get(this.global.url+'/getProduitByType.php?categorie=conso&&type=Lubrifiants')
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
            this.global.listeProduits = res;  
            console.log(res);
        }, (err) => {
          
          console.log(err);
  });
    this.http.get(this.global.url+'/getProduitByType.php?categorie=conso&&type=Produits blancs')
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
            this.global.listeProduitsBlanc = res;  
            console.log(res);
        }, (err) => {
          
          console.log(err);
        });
        this.http.get(this.global.url+'/getProduitByType.php?categorie=conso&&type=Gaz')
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
            this.global.listeProduitGaz = res;  
            console.log(res);
        }, (err) => {
          
          console.log(err);
        });

        this.http.get(this.global.url+'/getProduitByType.php?categorie=conso&&type=Accessoire')
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
            this.global.listeProduitAccessoire = res;  
            console.log(res);
        }, (err) => {
          
          console.log(err);
        });
        
        

  }
  goToPage(p){
    console.log(p.component);
    this.navCtrl.push(p.component);
  }
  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  verifierCode(){
    //this.ScanBarreCode();
    
    this.navCtrl.push(TypeVentePage);
    
  }


  ScanBarreCode(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.barcodeScanner.scan().then(barcodeData => {

      if(barcodeData.text != ""){
        this.http.get(this.global.url+'/checkCartByQrcode.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          loading.dismissAll();
          if(res == 0){
              this.global.presentAlert("message","Carte inexistante","ioio",VentePage,this);
          }else{
            this.http.get(this.global.url+'/getInfoParCarte.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client = res.client;
                this.global.MontantCarte = res.solde;
                this.global.carte_id = res.id;
                this.global.qrcode = res.qrcode;
                if(res.state == "terminee"){
                    this.global.presentAlert("Carte Perdu","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state == "expiree"){
                  this.global.presentAlert("Carte Expiree","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state ==  "suspendu"){
                  this.global.presentAlert("Carte Bloquée","Impossible de passser des transaction avec cette carte","fdffdf",VentePage,this);
                }if(res.state == "generee" || res.state == "activee"){
                  this.navCtrl.push(VentePage);
                }
                
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
      }else{
        loading.dismissAll();
      }
    
      
      }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
      });

}




  

}
