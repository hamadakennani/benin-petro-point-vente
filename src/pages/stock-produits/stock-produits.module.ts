import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StockProduitsPage } from './stock-produits';

@NgModule({
  declarations: [
    StockProduitsPage,
  ],
  imports: [
    IonicPageModule.forChild(StockProduitsPage),
  ],
})
export class StockProduitsPageModule {}
