import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the StockProduitsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stock-produits',
  templateUrl: 'stock-produits.html',
})
export class StockProduitsPage {
  liste_categorie = [
    {name : 'Lubrifiants'},
    {name : 'Produits blancs'},
    {name : 'Gaz'},
    {name : 'Accessoire'},
  ]
  listeProduit : any[];
  categorieProduit : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global : GlobalProvider,public loading: LoadingController) {
    this.categorieProduit = this.navParams.get("categorie");
    this.onChange();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StockProduitsPage');
  }

  
  logout(){
    this.global.logout2(this.navCtrl);
  }

  onChange(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/gerant/produitsParEmplacementCategorie.php?id='+this.global.agent_point_vente+'&&categorie='+this.categorieProduit+'&&fonction='+this.global.agent_fonction)
        .map(res => res.json())
        .subscribe(res => {
          this.listeProduit = res;
          loading.dismissAll();
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
  }

}
