import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { Http } from '@angular/http';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the CommandeProduitBlancPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commande-produit-blanc',
  templateUrl: 'commande-produit-blanc.html',
})
export class CommandeProduitBlancPage {

  montant : string= "";
  public idProduit :string;
  public prixProduit:string;
  public checkCodePin : boolean;
  public qte : string;
  public only_read:boolean;
  public immatriculation:string;
  public exist_produit = 0;
  public exist_point_vente = 0;
  constructor(public http: Http,public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController,public loading: LoadingController) {
    this.idProduit = navParams.get("idProduit");
    this.prixProduit = navParams.get("prixProduit");
    if(this.global.type_vente == "Vente par TV"){
      this.montant = this.global.MontantCarte;
      this.only_read = this.global.only_read;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommandeProduitBlancPage');
    document.getElementById("montant").focus();
  }

  logout(){
    this.global.logout();
  }

  commander(){
    if(this.montant != "" || parseFloat(this.montant) > 0){
        this.presentPrompt();
    }else{
        this.presentAlert("Obligatoire","Montant Obligatoire","erreur");
    }
    
  }

  presentAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.navCtrl.setRoot(HomePage);
          }

        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  presentPrompt() {
    if(parseFloat(this.montant) >0){
      if(this.global.type_vente == "Vente par TV"){
        let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
        loading.present();
        this.qte = ""+(parseFloat(this.montant) / parseFloat(this.prixProduit));
        
        this.http.get(this.global.url+'/checkQteEmplacement.php?point_vente='+this.global.agent_point_vente+'&&product_id='+this.idProduit)
          .map(res => res.json())
          .subscribe(res => {
          if(true){
          //loading.dismissAll();
          //var result_montant = String(parseFloat(this.montant) * parseFloat(this.qte));
          //if(parseFloat(this.global.MontantCarte) >= parseFloat(this.montant)){
            let postData = new FormData();
            postData.append("point_vente_id",this.global.agent_point_vente);
            postData.append("agent_id",this.global.agent_id);
            postData.append("carte_id",this.global.carte_id);
            postData.append("ticket_id",this.global.carte_id);
            postData.append("product_ids",this.idProduit);
            postData.append("montant",this.montant);
            postData.append("qte",this.qte);
            postData.append("type_vente",this.global.type_vente);
            postData.append("immatriculation",this.immatriculation);
            var hedears = new Headers();
            hedears.append("Content-Type","application/x-www-form-urlencoded");
            this.http.post(this.global.url+"/InsertTransaction.php",postData)
            .map(res => res.json())
            .subscribe(res => {
              loading.dismissAll();
              this.global.MontantCarte = String(parseFloat(this.global.MontantCarte) - parseFloat(this.montant));
              //this.app.getRootNav().push(HomePage);
              
              
              this.http.get(this.global.url+"/valideTicket.php?qrcode="+this.global.qrcode+"&&montant="+parseFloat(this.montant)+"&&point_vente="+this.global.agent_point_vente+"&&agent="+this.global.agent_id)
              .map(res => res.json())
              .subscribe(res => {
                this.idProduit = "";
                this.presentAlert('BRAVO',"Transaction effectuée avec succès.","success");
                
              }, (err) => {
                console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                console.log(err);
                loading.dismissAll();
              });
            }, (err) => {
              console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
              console.log(err);
              loading.dismissAll();
            });
            
         //}
         // else{
          //  this.SimpleAlert('Solde insuffisant',"Impossible de passer cette transaction","error");
          //  loading.dismissAll();
          //}
  
        }
        //else{
        //   this.SimpleAlert("Erreur","Quantité insufisante","erreur");
        //   loading.dismissAll();
        // }
      }, (err) => {
        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
        console.log(err);
        loading.dismissAll();
      });
      }
    if(this.global.type_vente == "Vente au comptant" || this.global.type_vente == "Vente par MONO PAY"){
      this.global.insertTransactionNormal(this.qte,this.montant,this.idProduit,this.prixProduit,this.navCtrl,"produit_blanc");
    }
    if(this.global.type_vente == "Vente par EASY CARD" || this.global.type_vente == "Vente par carte tampo"){
      
      
      let alert = this.alertCtrl.create({
        title: 'Code PIN<p id="montant_transaction" style="color:red;font-size:14px">Montant de la transaction : '+this.montant+'</p>',
        inputs: [
          {
            name: 'code',
            placeholder: 'Code PIN',
            type:"tel",
            id:"input_code_pin"
          }
        ],
        buttons: [
          {
            text: 'Valider',
            handler: data => {
              let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
              loading.present();
              this.http.get(this.global.url+'/checkValidateProduct.php?qrcode='+this.global.qrcode+'&&point_vente='+this.global.agent_point_vente+'&&produit='+this.idProduit)
                  .map(res => res.json())
                  .subscribe(res => {
                    console.log(res);
                    if(res.result_produit == 0){
                      this.SimpleAlert("Erreur","DÉSOLÉ CETTE CARTE NE PEUT PAS CONSOMMER LE PRODUITS QUE VOUS AVEZ CHOISIT","erreur");
                      loading.dismissAll();
                      return ;
                    }
                    if(res.result_point_vente == 0){
                      this.SimpleAlert("Erreur","DÉSOLÉ VOUS N'EST PAS AUTORISÉ À SERVIR AVEC CETTE CARTE","erreur");
                      loading.dismissAll();
                    }else{
                      this.InsertTransaction(this.global.qrcode,data.code,this.montant,this.idProduit,0,this.navCtrl,this.immatriculation);
                    }
                      
                  }, (err) => {
                    
                    console.log(err);
                    loading.dismissAll();
                  });
                  
                  
                  
            }
          },
          {
            text: 'Annuler',
            role: 'cancel',
            handler: data => {
              
            }
          }
        ]
      });
    alert.present();
  }
}
}

  checkCodePine(code_pin):any{
    //console.log(this.global.url+'/checkCodePin.php?qrcode=123456789&&code_pin=1234')
    return this.http.get(this.global.url+'/checkCodePin.php?qrcode=123456789&&code_pin='+code_pin)
          .map(res => res.json())
          .subscribe(res => {
              return res;
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
          });
  }




  InsertTransaction(qrCode,codepin,montant,idProduit,qte,nav,immatriculation){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/checkCodePin.php?qrcode='+qrCode+'&&code_pin='+codepin)
              .map(res => res.json())
              .subscribe(res => {
                  if(res == 1){
                    if(parseFloat(this.global.MontantCarte) >= parseFloat(montant)){
                      //var quantite = parseFloat(montant) / parseFloat(this.prixProduit)
                      qte = (parseFloat(montant) / parseFloat(this.prixProduit));
                      this.http.get(this.global.url+'/checkQteEmplacement.php?point_vente='+this.global.agent_point_vente+'&&product_id='+idProduit)
                      .map(res => res.json())
                      .subscribe(res => {
                      if(true){
                          let postData = new FormData();
                          postData.append("point_vente_id",this.global.agent_point_vente);
                          postData.append("agent_id",this.global.agent_id);
                          postData.append("carte_id",this.global.carte_id);
                          postData.append("ticket_id",this.global.carte_id);
                          postData.append("product_ids",idProduit);
                          postData.append("montant",montant);
                          postData.append("qte",qte);
                          postData.append("type_vente",this.global.type_vente);
                          postData.append("immatriculation",immatriculation);
                          var hedears = new Headers();
                          hedears.append("Content-Type","application/x-www-form-urlencoded");
                          this.http.post(this.global.url+"/InsertTransaction.php",postData)
                          .map(res => res.json())
                          .subscribe(res => {
                            loading.dismissAll();
                            this.presentAlert('BRAVO',"Transaction effectuée avec succès.","success");
                            this.global.MontantCarte = String(parseFloat(this.global.MontantCarte) - parseFloat(montant));
                            //this.app.getRootNav().push(HomePage);
                          }, (err) => {
                            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                            console.log(err);
                            loading.dismissAll();
                          });
                        }
                      //   else{
                      //     this.SimpleAlert("Erreur","Quantité insufisante","erreur");
                      //     loading.dismissAll();
                      // }
                  }, (err) => {
                    console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                    console.log(err);
                  });

                      //
                      
                    }
                    else{
                      this.SimpleAlert('Solde insuffisant',"Impossible de passer cette transaction","error");
                      montant = "";
                      loading.dismissAll();
                    }
      
                  }else{
                      this.SimpleAlert("Erreur","Code pin invalide","erreur");
                      loading.dismissAll();
                  }
              }, (err) => {
                console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                console.log(err);
                loading.dismissAll();
              });
  }



  SimpleAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.navCtrl.setRoot(HomePage);
          }
        }
      }]
    });
    alert.present();
  }

}
