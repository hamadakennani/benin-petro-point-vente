import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommandeProduitBlancPage } from './commande-produit-blanc';

@NgModule({
  declarations: [
    CommandeProduitBlancPage,
  ],
  imports: [
    IonicPageModule.forChild(CommandeProduitBlancPage),
  ],
})
export class CommandeProduitBlancPageModule {}
