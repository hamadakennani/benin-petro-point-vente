import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import "rxjs/add/operator/map"
import { CommandeProduitBlancPage } from '../commande-produit-blanc/commande-produit-blanc';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ProduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-produit',
  templateUrl: 'produit.html',

})
export class ProduitPage {

  list_a : any[];
  numbers : any[];
  buttons : Array<{title:string,component: any}>;
  constructor(public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http) {
    this.numbers = Array(5).fill(0).map((x,i)=>i);
    this.buttons = this.global.listeProduitsBlanc;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProduitPage');
    console.log(this.numbers);
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    
  }

  goToPage(idProduit,prix){
    this.navCtrl.push(CommandeProduitBlancPage,{
      "idProduit": idProduit,
      "prixProduit":prix
    });
  }

  logout(){
    this.global.logout();
  }
  

}
