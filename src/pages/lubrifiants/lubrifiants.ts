import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { CommandeGazPage } from '../commande-gaz/commande-gaz';
import { DetailProduitPage } from '../detail-produit/detail-produit';
import { CommandeProduitBlancPage } from '../commande-produit-blanc/commande-produit-blanc';


@IonicPage()
@Component({
  selector: 'page-lubrifiants',
  templateUrl: 'lubrifiants.html',
})
export class LubrifiantsPage {
  pet: string = "puppies";
  
  listeProduits = [ ]
  constructor(public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
    console.log("hhhhhhhhhhhhhhhhhhhhhhhhhhh");
    console.log(this.global.listeProduits);
    this.listeProduits = this.global.listeProduits;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LubrifiantsPage');
    console.log(this.listeProduits);
  }

  logout(){
    this.global.logout();
  }
  goToPage(idProduit,prix){
    this.navCtrl.push(CommandeProduitBlancPage,{
      "idProduit": idProduit,
      "prixProduit":prix
    });
  }
  
  pageCommande(name,prix,id_produit,existQte){
    event.stopPropagation();
    this.navCtrl.push(CommandeGazPage,{
      "id_produit":id_produit,
      "name":name,
      "prix":prix,
      "existQte":existQte
    });
  }

  pagedetail(event,id,name,prix,codeBarre){
    event.stopPropagation();
    this.navCtrl.push(DetailProduitPage,{
      "name":name,
      "prix":prix,
      "codeBarre":codeBarre
    });
  }


//   ScanBarreCode(){
//     var name : any
//     var prix: any
//     var codeBarre : any
//     var cpt:any
//     this.barcodeScanner.scan().then(barcodeData => {
//       cpt = 0;
//       if(barcodeData.text != ""){
//           this.listeProduits.forEach(function(a){
//             if(a.codeBarre == barcodeData.text){
//               name = a.name;
//               prix = a.prix;
//               codeBarre = a.codeBarre;
//               cpt++;
//               return false;
//             }
//           });
//           if(cpt >0 ){
//             this.navCtrl.push(CommandeGazPage,{
//               "name":name,
//               "prix":prix
//             });
//           }else{
//             this.global.presentAlert("Produit inexistant","Impossible de trouver ce produit","erreur",LubrifiantsPage,this);
//           }

//       }
//       }).catch(err => {
//       //    console.log('Error', err);
//       });

// }
}