import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LubrifiantsPage } from './lubrifiants';

@NgModule({
  declarations: [
    LubrifiantsPage,
  ],
  imports: [
    IonicPageModule.forChild(LubrifiantsPage),
  ],
})
export class LubrifiantsPageModule {}
