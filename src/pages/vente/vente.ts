import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Platform } from 'ionic-angular';
import { LubrifiantsPage } from '../lubrifiants/lubrifiants';
import { GazPage } from '../gaz/gaz';
import { ProduitPage } from '../produit/produit';
import { GlobalProvider } from '../../providers/global/global';
import { LavagePage } from '../lavage/lavage';

/**
 * Generated class for the VentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vente',
  templateUrl: 'vente.html',
})
export class VentePage {

  buttons : Array<{title:string,component: any}>;

  constructor(public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams,platform: Platform) {
    platform.registerBackButtonAction(() => {
      console.log("backPressed 1");
    },1);

    this.buttons = [
      {title: 'Produits Blancs',component:ProduitPage},
      {title: 'Lubrifiants',component:LubrifiantsPage},
      {title: 'Gaz',component:GazPage},
     // {title: 'Accessoires',component:AccessoirePage},
      {title: 'SERVICE ANNEXE',component:LavagePage},
    ]
    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VentePage');
    //var myInterval = setInterval(() => {
    //  document.getElementById("msg").style.display = "none";
     // clearInterval(myInterval); 
    //},2000);
  }

  goToPage(page){
    this.navCtrl.push(page.component);
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

}
