import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthentifiactionPage } from '../authentifiaction/authentifiaction';


@IonicPage()
@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstPage');
    var myInterval = setInterval(() => {
      this.navCtrl.setRoot(AuthentifiactionPage);
      clearInterval(myInterval); 
    },3000);
  }

}
