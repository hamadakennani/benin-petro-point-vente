import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodebarreTvPage } from './codebarre-tv';

@NgModule({
  declarations: [
    CodebarreTvPage,
  ],
  imports: [
    IonicPageModule.forChild(CodebarreTvPage),
  ],
})
export class CodebarreTvPageModule {}
