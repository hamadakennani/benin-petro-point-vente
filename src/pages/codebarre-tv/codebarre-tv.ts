import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { VentePage } from '../vente/vente';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';

/**
 * Generated class for the CodebarreTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-codebarre-tv',
  templateUrl: 'codebarre-tv.html',
})
export class CodebarreTvPage {

  public codebarre:string="";
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,public loading: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodebarreTvPage');
  }


  valider(){
      if(this.codebarre != ""){
        let loading = this.loading.create({content : "Logging in ,please wait..."});
        loading.present();
        this.http.get(this.global.url+'/existTicket.php?qrcode='+this.codebarre)
        .map(res => res.json())
        .subscribe(res => {
          if(res == 0){
              // this.navCtrl.push(VentePage);
              // this.global.MontantCarte = "";
              // this.global.client = "";
              // this.global.qrcode = this.codebarre;
              // this.global.only_read = false;
              // loading.dismissAll();
              this.global.presentAlert("message","Ticket inexistante","ioio",CodebarreTvPage,this);
              loading.dismissAll();
          }else{
            this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+this.codebarre)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client = res[0].client;
                this.global.MontantCarte = res[0].montant;
                this.global.carte_id = res[0].id;
                this.global.qrcode = res[0].qrcode;
                if(res[0].etat == 'util'){
                  this.global.presentAlert("Attention","Ce ticket a été dèjà utilisé dans le passé","ioio",CodebarreTvPage,this);
                }else{
                  this.navCtrl.push(VentePage);
                  this.global.only_read = true;

                }
                
                loading.dismissAll();
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });

  }
}

}
