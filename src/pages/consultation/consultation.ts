import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { DetailTransactionPage } from '../detail-transaction/detail-transaction';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-consultation',
  templateUrl: 'consultation.html',
})
export class ConsultationPage {

  listeTransaction : any[];
  valideShift:boolean=false;
  pet: string = "puppies";
  listeParCategorie : any[];
  listeParType : any[];
  montantTranscations : string;
  qteGasoilRemise : string="0";
  qteEssenceRemise : string="0";
  qtePetroleRemise : string="0";
  mntGasoilRemise : string="0";
  mntEssenceRemise : string="0";
  mntPetroleRemise : string="0";
  totalMntRemise : string="0";
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,private alertCtrl:AlertController,public loading: LoadingController) {
      this.ListeTransaction();
      this.valideShift = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultationPage');
  }

  logout(){
    this.global.logout();
  }

  getTotalParType(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/gerant/ventParTypeVente.php?agent_id='+this.global.agent_id+'&&point_vente_id='+this.global.agent_point_vente)
        .map(res => res.json())
        .subscribe(res => {
          console.log(res);
          this.listeParType = res;
          loading.dismissAll();
            
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
  }

  getTotalParCategorie(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/gerant/ventPaCategories.php?agent_id='+this.global.agent_id+'&&point_vente_id='+this.global.agent_point_vente+'&&choix=All')
        .map(res => res.json())
        .subscribe(res => {
          console.log(res);
          this.listeParCategorie = res;
          loading.dismissAll();
            
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
  }


  ListeTransaction(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/getListeTransactionByUser.php?user='+this.global.agent_id)
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);

          if(res){
            this.listeTransaction = res;  
            if(res[0]){
              this.valideShift = res[0].agent;
            }
            this.http.get(this.global.url+'/getMontantTransactionsNew.php?user='+this.global.agent_id)
            .map(res => res.json())
            .subscribe(res => {
              this.montantTranscations = res[0].montant;
              this.qteGasoilRemise = res[0].qteGasoilRemise;
              this.qteEssenceRemise = res[0].qteEssenceRemise;
              this.qtePetroleRemise = res[0].qtePetroleRemise;
              this.mntGasoilRemise = res[1].qteGasoilRemise;
              this.mntEssenceRemise = res[1].qteEssenceRemise;
              this.mntPetroleRemise = res[1].qtePetroleRemise;
              var total = parseFloat(res[1].qteGasoilRemise)+parseFloat(res[1].qteEssenceRemise)+parseFloat(res[1].qtePetroleRemise);
              this.totalMntRemise =""+total
            }, (err) => {
              
              console.log(err);
            });
            loading.dismissAll();
          }
            
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
        

  }

  goToPage(id){
    this.navCtrl.push(DetailTransactionPage,{
      "id":id
    })
  }


  ValiderShift() {
    
    if(this.listeTransaction.length > 0){
    if(this.valideShift){
      this.global.SimpleAlert('Validation du shift',"Ce shift est déjà validé","error",this);
    }else{
      let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
      loading.present();
      let alert = this.alertCtrl.create({
        title: 'Validation du shift',
        message: 'Voulez-vous vraiment valider ce shift',
        buttons: [
          {
            text: 'Confirmer',
            handler: () => {
              console.log('Buy clicked');
              this.http.get(this.global.url+'/valideTransactions.php?user='+this.global.agent_id+'&&type='+this.global.agent_fonction)
              .map(res => res.json())
              .subscribe(res => {
                  //this.ListeTransaction();
                  this.navCtrl.setRoot(HomePage);
                  loading.dismissAll();
              }, (err) => {
                
                console.log(err);
                loading.dismissAll();
              });
              
            }
          },
          {
            text: 'Annuler',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              loading.dismissAll();
            }
          }
        ]
      });
      alert.present();
    }
    
  }else{
    this.global.SimpleAlert('Validation du shift',"Aucune transaction a validé","error",this);
    //loading.dismissAll();
  }
}

}
