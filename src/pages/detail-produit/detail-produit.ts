import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailProduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-produit',
  templateUrl: 'detail-produit.html',
})
export class DetailProduitPage {

  name : string;
  prix : string;
  codeBarre : string
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.name = navParams.get("name");
    this.prix = navParams.get("prix");
    this.codeBarre = navParams.get("codeBarre");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailProduitPage');
  }

}
