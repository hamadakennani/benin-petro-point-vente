import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LavagePage } from './lavage';

@NgModule({
  declarations: [
    LavagePage,
  ],
  imports: [
    IonicPageModule.forChild(LavagePage),
  ],
})
export class LavagePageModule {}
