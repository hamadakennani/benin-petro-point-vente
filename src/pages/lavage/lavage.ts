import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { CommandeGazPage } from '../commande-gaz/commande-gaz';
import { CommandeProduitBlancPage } from '../commande-produit-blanc/commande-produit-blanc';

/**
 * Generated class for the LavagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lavage',
  templateUrl: 'lavage.html',
})
export class LavagePage {
  
  listes:any;
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,public loading: LoadingController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LavagePage');
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.global.url+'/getProduitByType.php?categorie=service')
    .map(res => res.json())
    .subscribe(res => {
        this.listes = res;
        loading.dismissAll();

    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }

  goToPage(idProduit,prix){
    this.navCtrl.push(CommandeProduitBlancPage,{
      "idProduit": idProduit,
      "prixProduit":prix
    });
  }

  pageCommande(name,prix,id_produit,existQte){
    event.stopPropagation();
    this.navCtrl.push(CommandeGazPage,{
      "id_produit":id_produit,
      "name":name,
      "prix":prix,
      "existQte":existQte
    });
  }

}
