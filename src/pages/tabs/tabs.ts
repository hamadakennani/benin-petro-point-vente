import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ProfilPage } from '../profil/profil';
import { GlobalProvider } from '../../providers/global/global';
import { GerantPage } from '../gerant/gerant';
import { ListeShiftPage } from '../liste-shift/liste-shift';
import { StockCategoriePage } from '../stock-categorie/stock-categorie';
import { VersementPage } from '../versement/versement';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProfilPage;
  tab3Root = GerantPage;
  tab4Root = ListeShiftPage;
  tab6Root = StockCategoriePage;
  tab7Root = VersementPage;

  gerant:string;
  constructor(public global:GlobalProvider) {
    if(this.global.agent_fonction == 'Pompiste'){
      this.gerant = "Pompiste";
    }
    else if (this.global.agent_fonction == 'Chauffeur') {
      this.gerant = "Chauffeur";
    }else{
      this.gerant = "Gerant";
    }

  }
}
