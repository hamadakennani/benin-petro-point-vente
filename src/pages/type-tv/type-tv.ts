import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { VentePage } from '../vente/vente';
import { CodebarreTvPage } from '../codebarre-tv/codebarre-tv';

/**
 * Generated class for the TypeTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type-tv',
  templateUrl: 'type-tv.html',
})
export class TypeTvPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,public http: Http,public loading: LoadingController,private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TypeTvPage');
  }

  goToPage(type){
    if(type == "scanner"){
      this.ScanBarreCodeTV();
    }else{
      this.navCtrl.push(CodebarreTvPage);
    }
  }


  ScanBarreCodeTV(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.barcodeScanner.scan().then(barcodeData => {

      if(barcodeData.text != ""){
        this.http.get(this.global.url+'/existTicket.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          loading.dismissAll();
          if(res == 0){
              // this.navCtrl.push(VentePage);
              // this.global.qrcode = barcodeData.text;
              // this.global.only_read = false;
              this.global.presentAlert("message","Ticket inexistante","ioio",TypeTvPage,this);
              loading.dismissAll();
          }else{
            this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client = res[0].client;
                this.global.MontantCarte = res[0].montant;
                this.global.carte_id = res[0].id;
                this.global.qrcode = res[0].qrcode;
                if(res[0].etat == 'util'){
                  this.global.presentAlert("Attention","Ce ticket a été dèjà utilisé dans le passé","ioio",TypeTvPage,this);
                }else{
                  this.navCtrl.push(VentePage);
                  this.global.only_read = true;

                }
                
                
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
      }else{
        loading.dismissAll();
      }
    
      
      }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
      });

}

}
