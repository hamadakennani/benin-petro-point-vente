import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { CommandeGazPage } from '../commande-gaz/commande-gaz';

/**
 * Generated class for the AccessoirePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accessoire',
  templateUrl: 'accessoire.html',
})
export class AccessoirePage {

  buttons : Array<{title:string,component: any}>;

  constructor(public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.buttons = this.global.listeProduitAccessoire;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccessoirePage');
  }

  logout(){
    this.global.logout();
  }

  goTopage(id,name,prix,existQte){
    this.navCtrl.push(CommandeGazPage,{
        "id_produit":id,
        "name":name,
        "prix":prix,
        "existQte":existQte
    });
  }

}
