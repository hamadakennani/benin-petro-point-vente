import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccessoirePage } from './accessoire';

@NgModule({
  declarations: [
    AccessoirePage,
  ],
  imports: [
    IonicPageModule.forChild(AccessoirePage),
  ],
})
export class AccessoirePageModule {}
