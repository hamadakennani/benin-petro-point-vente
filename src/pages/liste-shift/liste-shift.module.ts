import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListeShiftPage } from './liste-shift';

@NgModule({
  declarations: [
    ListeShiftPage,
  ],
  imports: [
    IonicPageModule.forChild(ListeShiftPage),
  ],
})
export class ListeShiftPageModule {}
