import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { DetailShiftPage } from '../detail-shift/detail-shift';

/**
 * Generated class for the ListeShiftPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liste-shift',
  templateUrl: 'liste-shift.html',
})
export class ListeShiftPage {
  listeShift : any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController) {
    this.ListeShift();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeShiftPage');
  }

  ListeShift(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/getListeShift.php?pointVente_id='+this.global.agent_point_vente)
        .map(res => res.json())
        .subscribe(res => {
          console.log(res);
          if(res){
            this.listeShift = res;  
          }
        loading.dismissAll(); 
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });

  }

  goToPage(id){
    this.navCtrl.push(DetailShiftPage,{
      "id":id
    });
  }


  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

}
