import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ListeProduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liste-produit',
  templateUrl: 'liste-produit.html',
})
export class ListeProduitPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,platform:Platform,public global:GlobalProvider) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeProduitPage');
  }

  logout(){
    this.global.logout();
  }

}
