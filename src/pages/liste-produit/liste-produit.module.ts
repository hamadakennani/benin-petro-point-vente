import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListeProduitPage } from './liste-produit';

@NgModule({
  declarations: [
    ListeProduitPage,
  ],
  imports: [
    IonicPageModule.forChild(ListeProduitPage),
  ],
})
export class ListeProduitPageModule {}
