import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GerantPage } from './gerant';

@NgModule({
  declarations: [
    GerantPage,
  ],
  imports: [
    IonicPageModule.forChild(GerantPage),
  ],
})
export class GerantPageModule {}
