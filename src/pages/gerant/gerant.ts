import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { DetailTransactionPage } from '../detail-transaction/detail-transaction';
import { ListeShiftPage } from '../liste-shift/liste-shift';

/**
 * Generated class for the GerantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gerant',
  templateUrl: 'gerant.html',
})
export class GerantPage {
  liste_pompiste : any[];
  listeTransaction : any[];
  montant_qte : any[];
  montantTranscations : string;
  qteGasoilRemise : string="0";
  qteEssenceRemise : string="0";
  qtePetroleRemise : string="0";
  mntGasoilRemise : string="0";
  mntEssenceRemise : string="0";
  mntPetroleRemise : string="0";
  totalMntRemise : string="0";
  agent : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,private alertCtrl:AlertController,public loading: LoadingController) {
    
    this.http.get(this.global.url+'/getListeAgentBypointVente.php?point_vente_id='+this.global.agent_point_vente +'&&id='+this.global.agent_id)
          .map(res => res.json())
          .subscribe(res => {
            this.liste_pompiste = res;
            console.log(this.liste_pompiste);
              
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GerantPage');
  }

  Actualiser(){
      this.onChange();
  }


  onChange(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/getListeTransactionByUser.php?user='+this.agent+'&&fonction=Gerant')
    .map(res => res.json())
    .subscribe(res => {
      console.log(res)
      this.listeTransaction = res;
      loading.dismissAll();
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });

    this.http.get(this.global.url+'/getMontantTransactionsNew.php?user='+this.agent)
        .map(res => res.json())
        .subscribe(res => {
            this.montantTranscations = res[0].montant;
            this.qteGasoilRemise = res[0].qteGasoilRemise;
            this.qteEssenceRemise = res[0].qteEssenceRemise;
            this.qtePetroleRemise = res[0].qtePetroleRemise;
            this.mntGasoilRemise = res[1].qteGasoilRemise;
            this.mntEssenceRemise = res[1].qteEssenceRemise;
            this.mntPetroleRemise = res[1].qtePetroleRemise;
            var total = parseFloat(res[1].qteGasoilRemise)+parseFloat(res[1].qteEssenceRemise)+parseFloat(res[1].qtePetroleRemise);
            this.totalMntRemise =""+total
            console.log(res);
        }, (err) => {
          
          console.log(err);
        });
  }
  goToPage(id){
    this.navCtrl.push(DetailTransactionPage,{
      "id":id
    });
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Code PIN',
      inputs: [
        {
          name: 'code',
          placeholder: 'Code PIN',
          type:"tel",
          id:"input_code_pin"
        }
      ],
      buttons: [
        {
          text: 'Valider',
          handler: data => {
            this.ValiderShift(data.code);
            
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            
          }
        }
      ]
    });
    alert.present();
  }

  ValiderShift(password){
    var transaction = this.listeTransaction.find(c => c.state === "valider");
    if(transaction != null){
      console.log(this.listeTransaction.find(c => c.state === "valider").length);
    }else{
      console.log("nnnnnnnnnnnnnnn");
    }
    if(transaction != null){
    if(this.listeTransaction.length > 0){
      
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/checkValideAgent.php?user='+this.agent)
    .map(res => res.json())
    .subscribe(res => {
      if(res == 0){
        let alert = this.alertCtrl.create({
          title: 'Validation du shift',
          message: 'Voulez-vous vraiment valider ce shift ?',
          buttons: [
            {
              text: 'Confirmer',
              handler: () => {
                console.log('Buy clicked');
                let alert = this.alertCtrl.create({
                  title: 'Code PIN',
                  inputs: [
                    {
                      name: 'code',
                      placeholder: 'Code PIN',
                      type:"tel",
                      id:"input_code_pin"
                    }
                  ],
                  buttons: [
                    {
                      text: 'Valider',
                      handler: data => {
                        this.http.get(this.global.url+'/checkCodePinByAgentId.php?id='+this.global.agent_id+'&&password='+data.code)
                        .map(res => res.json())
                        .subscribe(res => {
                          if(res == 1){
                        this.http.get(this.global.url+'/valideTransactions.php?user='+this.agent+'&&type='+this.global.agent_fonction+'&&gerant_id='+this.global.agent_id+'&&total='+this.montantTranscations)
                        .map(res => res.json())
                        .subscribe(res => {
                          this.onChange();
                          loading.dismissAll();
                          this.navCtrl.setRoot(ListeShiftPage);
                        }, (err) => {
                          
                          console.log(err);
                          loading.dismissAll();
                        });
                      }else{
                        this.global.SimpleAlert("Erreur","Code PIN invalide","erreur",this);
                        loading.dismissAll();
                      }
                      }, (err) => {
                          
                        console.log(err);
                        loading.dismissAll();
                      });
                        
                      }
                    },
                    {
                      text: 'Annuler',
                      role: 'cancel',
                      handler: data => {
                        loading.dismissAll();
                      }
                    }
                  ]
                });
                alert.present();
                
              }
            },
            {
              text: 'Annuler',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
                loading.dismissAll();
              }
            }
          ]
        });
        alert.present();
        
      }else{
        this.global.SimpleAlert('Validation du shift',"Alert : Ce shift n'est pas encore validée par l'agent","error",this);
        loading.dismissAll();
      }
    }, (err) => {
      
      console.log(err);
    }); 
    
    }else{
      this.global.SimpleAlert('Validation du shift',"Aucune transaction a validé","error",this);
    }
  }else{
    this.global.SimpleAlert('Validation du shift',"Aucune transaction a validé","error",this);
  }
  }

}
