import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { CommandeProduitBlancPage } from '../commande-produit-blanc/commande-produit-blanc';

/**
 * Generated class for the GazPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gaz',
  templateUrl: 'gaz.html',
})
export class GazPage {
  pet: string = "puppies";
  buttons : Array<{title:string,component: any}>;
  constructor(public global: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.buttons = this.global.listeProduitGaz;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GazPage');
  }

  logout(){
    this.global.logout();
  }
  
  goToPage(idProduit,prix){
    this.navCtrl.push(CommandeProduitBlancPage,{
      "idProduit": idProduit,
      "prixProduit":prix
    });
  }

  // goTopage(id,name,prix,existQte){
  //   this.navCtrl.push(CommandeGazPage,{
  //       "id_produit":id,
  //       "name":name,
  //       "prix":prix,
  //       "existQte":existQte
  //   });
  //}

}
