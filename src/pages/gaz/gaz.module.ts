import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GazPage } from './gaz';

@NgModule({
  declarations: [
    GazPage,
  ],
  imports: [
    IonicPageModule.forChild(GazPage),
  ],
})
export class GazPageModule {}
