import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { TestPage } from '../test/test';

@IonicPage()
@Component({
  selector: 'page-authentifiaction',
  templateUrl: 'authentifiaction.html',
})
export class AuthentifiactionPage {
  user : string;
  password : string;
  check : boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,private storage: Storage,public loading: LoadingController) {
    
    //this.navCtrl.setRoot(TabsPage);
    // Or to get a key/value pair
    this.storage.get('login').then((val) => {
      //console.log('Your age is', val);
      this.user = val
    });

  
  }

  ionViewDidLoad() {
    
    
  }

  login2(){
    this.navCtrl.setRoot(TestPage);

  }
  login(){
    
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    let postData = new FormData();
    postData.append("matricule",this.user);
    postData.append("password",this.password);
    var hedears = new Headers();
    hedears.append("Content-Type","application/x-www-form-urlencoded");
    hedears.append("Access-Control-Allow-Origin", "*");
    hedears.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    hedears.append('Access-Control-Allow-Origin' , '*');
    hedears.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    hedears.append('Accept','application/json');
    hedears.append('content-type','application/json');
    this.http.post(this.global.url+"/checkAgentByMatricule.php",postData)
    .map(res => res.json())
    .subscribe(res => {
      if(res == 1){
          this.check = true;
          this.storage.set('login', this.user);
          this.http.get(this.global.url+'/getAgentByMatricul.php?matricule='+this.user)
          .map(res => res.json())
          .subscribe(res => {
              this.global.agent_id = res.id;
              this.global.agent_point_vente = res.point_vente;
              this.global.agent_fonction = res.fonction
              loading.dismissAll();
              if(res.fonction == "Gerant"){
                  this.navCtrl.setRoot(TabsPage);
              }else{
                this.navCtrl.setRoot(TabsPage);
              }
              
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            this.navCtrl.setRoot(AuthentifiactionPage);
            //console.log(err);
          });
      }else{
          this.check = false;
          loading.dismissAll();
      }
      console.log(res);
      

    }, (err) => {
      console.log("hhhhhhhhhhhhhhhhhhhhhhhh");
      console.log(err);
      this.global.presentAlert('Erreur',err,"error",AuthentifiactionPage,this);
      loading.dismissAll();
      //console.log(err);
    });
  }

}
