import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthentifiactionPage } from './authentifiaction';

@NgModule({
  declarations: [
    AuthentifiactionPage,
  ],
  imports: [
    IonicPageModule.forChild(AuthentifiactionPage),
  ],
})
export class AuthentifiactionPageModule {}
