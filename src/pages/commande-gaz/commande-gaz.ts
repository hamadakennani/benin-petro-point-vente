import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { Http } from '@angular/http';

/**
 * Generated class for the CommandeGazPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commande-gaz',
  templateUrl: 'commande-gaz.html',
})
export class CommandeGazPage {

  id_produit :string
  produit:string
  montant:string
  quantite:any
  montCommande:string
  checkQte:boolean = true;
  public immatriculation:string;
  public exist_produit = 0;
  public exist_point_vente = 0;
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,private alertCtrl:AlertController,public loading: LoadingController) {
    this.produit = navParams.get("name");
    this.montant = navParams.get("prix");
    this.id_produit = navParams.get("id_produit");
    if(this.navParams.get("existQte") == 1){
      this.checkQte = true;
    }else{
      this.checkQte = false;
    }
    this.quantite = 1;

  }

  logout(){
    this.global.logout();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommandeGazPage');
  }

  commander(){
    this.presentPrompt();
  }


  presentPrompt() {
    if(this.global.type_vente == "Vente par TV"){
      let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
      loading.present();
      this.http.get(this.global.url+'/checkQteEmplacement.php?point_vente='+this.global.agent_point_vente+'&&product_id='+this.id_produit)
        .map(res => res.json())
        .subscribe(res => {
        if(parseFloat(res) >= parseFloat(this.quantite)){
        //loading.dismissAll();
        result_montant = String(parseFloat(this.montant) * parseFloat(this.quantite));
        if (this.global.only_read == false){
          this.global.MontantCarte = result_montant;
        }
        if(parseFloat(this.global.MontantCarte) >= parseFloat(result_montant)){
          let postData = new FormData();
          postData.append("point_vente_id",this.global.agent_point_vente);
          postData.append("agent_id",this.global.agent_id);
          postData.append("carte_id",this.global.carte_id);
          postData.append("ticket_id",this.global.carte_id);
          postData.append("product_ids",this.id_produit);
          postData.append("montant",result_montant);
          postData.append("qte",this.quantite);
          postData.append("type_vente",this.global.type_vente);
          postData.append("immatriculation",this.immatriculation);
          var hedears = new Headers();
          hedears.append("Content-Type","application/x-www-form-urlencoded");
          this.http.post(this.global.url+"/InsertTransaction.php",postData)
          .map(res => res.json())
          .subscribe(res => {
            loading.dismissAll();
            this.global.MontantCarte = String(parseFloat(this.global.MontantCarte) - parseFloat(result_montant));
            //this.app.getRootNav().push(HomePage);
            this.http.get(this.global.url+"/valideTicket.php?qrcode="+this.global.qrcode+"&&montant="+parseFloat(result_montant)+"&&point_vente="+this.global.agent_point_vente+"&&agent="+this.global.agent_id)
            .map(res => res.json())
            .subscribe(res => {
              
              this.presentAlert('BRAVO',"Transaction effectuée avec succès.","success");
              
            }, (err) => {
              console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
              console.log(err);
              loading.dismissAll();
            });
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            console.log(err);
            loading.dismissAll();
          });
          //
          
        }
        else{
          this.SimpleAlert('Solde insuffisant',"Impossible de passer cette transaction","error");
          loading.dismissAll();
        }

      }else{
        this.SimpleAlert("Erreur","Quantité insufisante","erreur");
        loading.dismissAll();
      }
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
    }
    if(this.global.type_vente == "Vente au comptant" || this.global.type_vente == "Vente par MONO PAY"){
      var result_montant;
      result_montant = String(parseFloat(this.montant) * parseFloat(this.quantite));
      this.global.insertTransactionNormal(parseInt(this.quantite),result_montant,this.id_produit,this.montant,this.navCtrl,"autre");
    }
    if(this.global.type_vente == "Vente par EASY CARD" || this.global.type_vente == "Vente par carte tampo"){
      
      let alert = this.alertCtrl.create({
      title: 'Code PIN<p id="montant_transaction" style="color:red;font-size:14px">Montant de la transaction : '+String(parseFloat(this.montant) * parseFloat(this.quantite))+'</p>',
      inputs: [
        {
          name: 'code',
          placeholder: 'code pin',
          type:"tel",
          id:"input_code_pin"
        }
      ],
      buttons: [
        {
          text: 'Valider',
          handler: data => {
            let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
            loading.present();
            this.http.get(this.global.url+'/checkValidateProduct.php?qrcode='+this.global.qrcode+'&&point_vente='+this.global.agent_point_vente+'&&produit='+this.id_produit)
                .map(res => res.json())
                .subscribe(res => {
                  console.log(res);
                  this.exist_produit = res.result_produit;
                  this.exist_point_vente = res.result_point_vente;
                  
                  loading.dismissAll();
                    
                }, (err) => {
                  
                  console.log(err);
                  loading.dismissAll();
                });
            loading.present();
            var result_montant;
            result_montant = String(parseFloat(this.montant) * parseFloat(this.quantite));
            if(parseInt(this.quantite) > 0){
              if(parseFloat(result_montant) > 0 ){
                this.http.get(this.global.url+'/checkQteEmplacement.php?point_vente='+this.global.agent_point_vente+'&&product_id='+this.id_produit)
                .map(res => res.json())
                .subscribe(res => {
                    if(parseFloat(res) >= parseInt(this.quantite)){
                      if(this.exist_produit == 0){
                        this.SimpleAlert("Erreur","DÉSOLÉ CETTE CARTE NE PEUT PAS CONSOMMER LE PRODUITS QUE VOUS AVEZ CHOISIT","erreur");
                        return ;
                      }
                      if(this.exist_point_vente == 0){
                        this.SimpleAlert("Erreur","DÉSOLÉ VOUS N'EST PAS AUTORISÉ À SERVIR AVEC CETTE CARTE","erreur");
                      }else{
                        this.InsertTransaction(this.global.qrcode,data.code,result_montant,this.id_produit,parseInt(this.quantite),this.navCtrl,this.immatriculation);
                      }
                      }else{
                        this.SimpleAlert("Erreur","Quantité insufisante","erreur");
                        loading.dismissAll();
                    }
                }, (err) => {
                  console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                  console.log(err);
                });
              }else{
                  this.SimpleAlert('Erreur',"Impossible de passer cette transaction","error");
              }
            }else{
                this.SimpleAlert('Erreur',"Impossible de passer cette transaction","error");
            }
            
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            
          }
        }
      ]
    });
    alert.present();
  }
}

  presentAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.navCtrl.setRoot(HomePage);
          }
        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }


  InsertTransaction(qrCode,codepin,montant,idProduit,qte,nav,immatriculation){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+'/checkCodePin.php?qrcode='+qrCode+'&&code_pin='+codepin)
              .map(res => res.json())
              .subscribe(res => {
                  if(res == 1){
                    this.http.get(this.global.url+'/checkQteEmplacement.php?point_vente='+this.global.agent_point_vente+'&&product_id='+idProduit)
                    .map(res => res.json())
                    .subscribe(res => {
                    if(parseFloat(res) >= parseFloat(qte)){
                    //loading.dismissAll();
                    if(parseFloat(this.global.MontantCarte) >= parseFloat(montant)){
                      let postData = new FormData();
                      postData.append("point_vente_id",this.global.agent_point_vente);
                      postData.append("agent_id",this.global.agent_id);
                      postData.append("carte_id",this.global.carte_id);
                      postData.append("ticket_id","");
                      postData.append("product_ids",idProduit);
                      postData.append("montant",montant);
                      postData.append("qte",qte);
                      postData.append("type_vente",this.global.type_vente);
                      postData.append("immatriculation",immatriculation);
                      var hedears = new Headers();
                      hedears.append("Content-Type","application/x-www-form-urlencoded");
                      this.http.post(this.global.url+"/InsertTransaction.php",postData)
                      .map(res => res.json())
                      .subscribe(res => {
                        loading.dismissAll();
                        this.presentAlert('BRAVO',"Transaction effectuée avec succès.","success");
                        this.global.MontantCarte = String(parseFloat(this.global.MontantCarte) - parseFloat(montant));
                        //this.app.getRootNav().push(HomePage);
                      }, (err) => {
                        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                        console.log(err);
                        loading.dismissAll();
                      });
                      //
                      
                    }
                    else{
                      this.SimpleAlert('Solde insuffisant',"Impossible de passer cette transaction","error");
                      montant = "";
                      loading.dismissAll();
                    }
      
                  }else{
                    this.SimpleAlert("Erreur","Quantité insufisante","erreur");
                    loading.dismissAll();
                  }
                }, (err) => {
                  console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                  console.log(err);
                  loading.dismissAll();
                });










                  }else{
                      this.SimpleAlert("Erreur","Code pin invalide","erreur");
                      loading.dismissAll();
                  }
              }, (err) => {
                console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                console.log(err);
                loading.dismissAll();
              });
  }

  SimpleAlert(title,text,type) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.navCtrl.setRoot(HomePage);
          }
        }
      }]
    });
    alert.present();
  }

}
