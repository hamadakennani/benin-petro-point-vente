import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommandeGazPage } from './commande-gaz';

@NgModule({
  declarations: [
    CommandeGazPage,
  ],
  imports: [
    IonicPageModule.forChild(CommandeGazPage),
  ],
})
export class CommandeGazPageModule {}
