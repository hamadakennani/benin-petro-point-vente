import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';

/**
 * Generated class for the VersementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-versement',
  templateUrl: 'versement.html',
})
export class VersementPage {
  type_action:string="liste";
  typeProduit:string="";
  banque:string="";
  num_versement:string="";
  montant_versement:string;
  liste_journal:any[];
  liste_versement:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,public http: Http,public loading: LoadingController,private alertCtrl:AlertController) {
    this.montant_versement = "";
    this.banque= "";
    this.typeProduit = "";
    this.num_versement = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VersementPage');
    this.type_action = "liste";
    this.getListeVersement();
  }

  getListeVersement(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+"/gerant/getListeVersement.php?point_vente_id="+this.global.agent_point_vente)
    //this.http.get(this.global.url+"/gerant/getListeVersement.php?point_vente_id=177")
    .map(res => res.json())
      .subscribe(res => {
      //alert(res);
      this.liste_versement = res;
      loading.dismissAll();
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }

  addVersement(){
    this.montant_versement = "";
    this.banque= "";
    this.typeProduit = "";
    this.num_versement = "";
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.type_action = "add";
    this.http.get(this.global.url+"/gerant/getListeAllJournal.php")
    //this.http.get(this.global.url+"/gerant/getListeVersement.php?point_vente_id=177")
    .map(res => res.json())
      .subscribe(res => {
      //alert(res);
      this.liste_journal = res;
      loading.dismissAll();
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  onChange(){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    this.http.get(this.global.url+"/gerant/ventPaCategories.php?point_vente_id="+this.global.agent_point_vente+"&&choix="+this.typeProduit)
    .map(res => res.json())
      .subscribe(res => {
      if(res.length != 0){
        this.montant_versement = res[0].montant;
      }else{
        this.montant_versement = "0";
      }
      
      loading.dismissAll();
    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
      loading.dismissAll();
    });
  }


  listVersement(){
    this.type_action = "liste";
    this.getListeVersement();
  }
  detailVersement(){
    this.type_action = "detail";
  }

  enregistrer(){
    if(this.typeProduit != "" && this.num_versement != "" && this.banque != ""){
    if(parseFloat(this.montant_versement) > 0){
      let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
      loading.present();
      let postData = new FormData();
      postData.append("point_vente_id",this.global.agent_point_vente);
      postData.append("journal_id",this.banque);
      postData.append("num_versement",this.num_versement);
      postData.append("type_versement",this.typeProduit);
      postData.append("vers_par","gerant");
      postData.append("agent_id",this.global.agent_id);
      postData.append("montant",this.montant_versement);
      var hedears = new Headers();
      hedears.append("Content-Type","application/x-www-form-urlencoded");
      this.http.post(this.global.url+"/gerant/InsertVersement.php",postData)
      .map(res => res.json())
      .subscribe(res => {
        loading.dismissAll();
        this.SimpleAlert('BRAVO',"Transaction effectuée avec succès.","success",this.navCtrl);
      }, (err) => {
        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
        console.log(err);
        loading.dismissAll();
      });
    }else{
        this.SimpleAlert('Erreur',"Impossible d'enregistrer ce versement avec montant 0.","erreur",this.navCtrl);
    }
  }else{
    this.SimpleAlert('Erreur',"Merci de remplir tous les champs SVP","erreur",this.navCtrl);
  }
    
  }


  SimpleAlert(title,text,type,root) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.type_action = "liste";
            this.getListeVersement();
          }
        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }

}
