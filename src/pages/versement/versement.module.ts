import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VersementPage } from './versement';

@NgModule({
  declarations: [
    VersementPage,
  ],
  imports: [
    IonicPageModule.forChild(VersementPage),
  ],
})
export class VersementPageModule {}
