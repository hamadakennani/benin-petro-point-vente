import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailShiftPage } from './detail-shift';

@NgModule({
  declarations: [
    DetailShiftPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailShiftPage),
  ],
})
export class DetailShiftPageModule {}
