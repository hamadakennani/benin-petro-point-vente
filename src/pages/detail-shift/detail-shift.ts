import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the DetailShiftPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-shift',
  templateUrl: 'detail-shift.html',
})
export class DetailShiftPage {

  listeTransaction:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider) {
    this.listeTransactionByShift(navParams.get("id"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailShiftPage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  
  listeTransactionByShift(id){
    this.http.get(this.global.url+'/getListeTransactionByshift.php?id='+id)
        .map(res => res.json())
        .subscribe(res => {
          //this.global.presentAlert("message",res.client,"success",VentePage,this);
          console.log(res);
          if(res){
            this.listeTransaction = res;  
          }
            
        }, (err) => {
          
          console.log(err);
        });
  }

  

}
