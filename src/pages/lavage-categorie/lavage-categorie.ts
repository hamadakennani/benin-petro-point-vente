import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { CommandeGazPage } from '../commande-gaz/commande-gaz';


/**
 * Generated class for the LavageCategoriePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lavage-categorie',
  templateUrl: 'lavage-categorie.html',
})
export class LavageCategoriePage {

  type:any
  name:any
  listes:any;
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider) {
    this.type = navParams.get("type");
    this.http.get(this.global.url+'/getProduitByType.php?categorie=service&&type='+this.type)
    .map(res => res.json())
    .subscribe(res => {
        this.listes = res;

    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      console.log(err);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LavageCategoriePage');
  }

  pageCommande(name,prix,id_produit,existQte){
    event.stopPropagation();
    this.navCtrl.push(CommandeGazPage,{
      "id_produit":id_produit,
      "name":name,
      "prix":prix,
      "existQte":existQte
    });
  }

}
