import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LavageCategoriePage } from './lavage-categorie';

@NgModule({
  declarations: [
    LavageCategoriePage,
  ],
  imports: [
    IonicPageModule.forChild(LavageCategoriePage),
  ],
})
export class LavageCategoriePageModule {}
