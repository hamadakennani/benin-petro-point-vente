import { Injectable } from '@angular/core';
import { App, AlertController } from "ionic-angular";
import { NavController } from "ionic-angular/index";
import { AuthentifiactionPage } from '../../pages/authentifiaction/authentifiaction';
import { Http } from '@angular/http';
import { HomePage } from '../../pages/home/home';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class GlobalProvider {
  public only_read:boolean=false;
  public MontantCarte: string;
  public client: string;
  public carte_id:string;
  public qrcode:string;
  public navCtrl: NavController;
  public url : string = 'http://51.38.199.241/api';
  //public url : string = 'http://178.33.146.16/api';
  public listeProduits : any[];
  public listeProduitsBlanc : any[];
  public listeProduitGaz : any[];
  public listeProduitAccessoire : any[];
  public listeTransaction : any[];
  public agent_id:string;
  public agent_point_vente : string;
  public agent_fonction : string;
  public transfert_id : string;
  public flag:boolean=false;
  public signture1 :string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK0AAABqCAIAAABBIEYYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIISURBVHhe7djBjQJBFAPRyT/pBg5IaARUAH579cnlEtN/r+MPgXMuEBB4EuABDV4EeMADHnDgTcDvARf8HnDA7wEHPgn4LvDBd4EDvgsc8F3gwJ2A9wEnvA844H3AAe8DDngfcOAbAe9EXngncsA7kQPeiRzwTuSAdyIHfhFwL3DDvcAB9wIH3AsccC9wwL3AAfcCB/4RcDfyw93IAXcjB9yNHHA3csDdyAF3IwfcjRwoAv5/UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwtHzVfG6U/pf1sAAAAAElFTkSuQmCC";
  public signture2 :string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK0AAABqCAIAAABBIEYYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIISURBVHhe7djBjQJBFAPRyT/pBg5IaARUAH579cnlEtN/r+MPgXMuEBB4EuABDV4EeMADHnDgTcDvARf8HnDA7wEHPgn4LvDBd4EDvgsc8F3gwJ2A9wEnvA844H3AAe8DDngfcOAbAe9EXngncsA7kQPeiRzwTuSAdyIHfhFwL3DDvcAB9wIH3AsccC9wwL3AAfcCB/4RcDfyw93IAXcjB9yNHHA3csDdyAF3IwfcjRwoAv5/UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwtHzVfG6U/pf1sAAAAAElFTkSuQmCC";
  public type_vente : string;
  public chaffeur : string;
  public receptionnaire : string;
  public consomateur : string; 
  constructor(public http: Http,private app:App,private alertCtrl:AlertController,public loading: LoadingController) {
    console.log('Hello GlobalProvider Provider');
    this.MontantCarte = ''
    this.client= ''
  }

  logout() {
    //this.navCtrl.push(AuthentifiactionPage);
    this.presentConfirm();
    
  } 
  logout2(root) {
    //this.navCtrl.push(AuthentifiactionPage);
    let alert = this.alertCtrl.create({
      title: 'Fermeture de la session',
      message: 'voulez-vous vraiment quitter la session ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            console.log('Buy clicked');
            //root.setRoot(AuthentifiactionPage);
            this.app.getRootNav().setRoot(AuthentifiactionPage);
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
    
    
  } 
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Fermeture de la session',
      message: 'voulez-vous vraiment quitter la session ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            console.log('Buy clicked');
            this.app.getRootNav().setRoot(AuthentifiactionPage);
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
  presentAlert(title,text,type,page,root) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            root.navCtrl.setRoot(page);
          }
        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }


  SimpleAlert(title,text,type,root) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            this.app.getRootNav().setRoot(HomePage);
          }
        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  insertTransactionNormal(qte,montant,idProduit,prixProduit,nav,typeProduit){
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    if(typeProduit == "produit_blanc")
      qte = (parseFloat(montant) / parseFloat(prixProduit)).toFixed(2);
      
          this.http.get(this.url+'/checkQteEmplacement.php?point_vente='+this.agent_point_vente+'&&product_id='+idProduit)
          .map(res => res.json())
          .subscribe(res => {
          if(parseFloat(res) >= parseFloat(qte)){
              let postData = new FormData();
              postData.append("point_vente_id",this.agent_point_vente);
              postData.append("agent_id",this.agent_id);
              postData.append("carte_id",this.carte_id);
              postData.append("ticket_id","");
              postData.append("product_ids",idProduit);
              postData.append("montant",montant);
              postData.append("qte",qte);
              postData.append("type_vente",this.type_vente);
              var hedears = new Headers();
              hedears.append("Content-Type","application/x-www-form-urlencoded");
              this.http.post(this.url+"/InsertTransaction.php",postData)
              .map(res => res.json())
              .subscribe(res => {
                loading.dismissAll();
                this.SimpleAlert('BRAVO',"Transaction effectuée avec succès.","success",nav);
                //this.global.MontantCarte = String(parseFloat(this.global.MontantCarte) - parseFloat(this.montant));
                //this.app.getRootNav().push(HomePage);
              }, (err) => {
                console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                console.log(err);
                loading.dismissAll();
              });
            }else{
              this.SimpleAlert("Erreur","Quantité insufisante","erreur",nav);
              loading.dismissAll();
          }
      }, (err) => {
        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
        console.log(err);
      });
}
  
  InsertTransaction(qrCode,codepin,montant,idProduit,qte,nav){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.url+'/checkCodePin.php?qrcode='+qrCode+'&&code_pin='+codepin)
              .map(res => res.json())
              .subscribe(res => {
                  if(res == 1){

                    loading.dismissAll();
                    if(parseFloat(this.MontantCarte) >= parseFloat(montant)){
                      let postData = new FormData();
                      postData.append("point_vente_id",this.agent_point_vente);
                      postData.append("agent_id",this.agent_id);
                      postData.append("carte_id",this.carte_id);
                      postData.append("ticket_id","");
                      postData.append("product_ids",idProduit);
                      postData.append("montant",montant);
                      postData.append("qte",qte);
                      var hedears = new Headers();
                      hedears.append("Content-Type","application/x-www-form-urlencoded");
                      this.http.post(this.url+"/InsertTransaction.php",postData)
                      .map(res => res.json())
                      .subscribe(res => {
                        this.presentAlert('BRAVO',"Transaction effectuée avec succès.","success",HomePage,nav);
                        this.MontantCarte = String(parseFloat(this.MontantCarte) - parseFloat(montant));
                        //this.app.getRootNav().push(HomePage);
                      }, (err) => {
                        console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                        console.log(err);
                      });
                      //
                      
                    }
                    else{
                      this.SimpleAlert('Solde insuffisant',"Impossible de passer cette transaction","error",nav);
                      montant = "";
                    }
      
                  }else{
                      this.SimpleAlert("Erreur","Code pin invalide","erreur",nav);
                  }
              }, (err) => {
                console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                console.log(err);
                loading.dismissAll();
              });
  }
  

}
