import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FirstPage } from '../pages/first/first';
import { GlobalProvider } from '../providers/global/global';
import { GerantPage } from '../pages/gerant/gerant';
import { ListeShiftPage } from '../pages/liste-shift/liste-shift';
import { StockCategoriePage } from '../pages/stock-categorie/stock-categorie';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = FirstPage;
  montantCarte : any
  agent: boolean;
  menus = [
    {title : 'Validation du shift',component:GerantPage},
    {title : 'Shifts validés',component:ListeShiftPage},
    {title : 'Liste des produits',component:StockCategoriePage}
  ]
  gerant:string;
  @ViewChild(Nav) nav: Nav;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public global:GlobalProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      //splashScreen.hide();
      
    });
    if(this.global.agent_fonction == 'Pompiste'){
      this.gerant = "Pompiste";
    }
    else if (this.global.agent_fonction == 'Chauffeur') {
      this.gerant = "Chauffeur";
    }else{
      this.gerant = "Gerant";
    }


    this.montantCarte = 2500
  }
  openShare(){
  
  }

  
  logout(){
    this.global.logout();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

  goPage(m){
    if(m == 'vs')
      this.nav.push(GerantPage);
    if(m == 'lsv')
      this.nav.push(ListeShiftPage);
    if(m == 'lp')
      this.nav.push(StockCategoriePage);
    //this.nav.push(m.component);
  }
}
