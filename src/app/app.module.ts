import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthentifiactionPage } from '../pages/authentifiaction/authentifiaction';
import { ProduitPage } from '../pages/produit/produit';
import { HttpModule } from '@angular/http';
import { VentePage } from '../pages/vente/vente';
import { ProfilPage } from '../pages/profil/profil';
import { ListeProduitPage } from '../pages/liste-produit/liste-produit';
import { LubrifiantsPage } from '../pages/lubrifiants/lubrifiants';
import { GazPage } from '../pages/gaz/gaz';
import { ConsultationPage } from '../pages/consultation/consultation';
import { CommandeProduitBlancPage } from '../pages/commande-produit-blanc/commande-produit-blanc';
import { GlobalProvider } from '../providers/global/global';
import { HttpClientModule } from '@angular/common/http'; 
import { FirstPage } from '../pages/first/first';
import { CommandeGazPage } from '../pages/commande-gaz/commande-gaz';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SearchPipe } from '../pipes/search/search';
import { DetailProduitPage } from '../pages/detail-produit/detail-produit';
import { LavagePage } from '../pages/lavage/lavage';
import { LavageCategoriePage } from '../pages/lavage-categorie/lavage-categorie';
import { IonicStorageModule } from '@ionic/storage';
import { DetailTransactionPage } from '../pages/detail-transaction/detail-transaction';
import { GerantPage } from '../pages/gerant/gerant';
import { ListeShiftPage } from '../pages/liste-shift/liste-shift';
import { DetailShiftPage } from '../pages/detail-shift/detail-shift';
import { AccessoirePage } from '../pages/accessoire/accessoire';
import { StockProduitsPage } from '../pages/stock-produits/stock-produits';
import { StockCategoriePage } from '../pages/stock-categorie/stock-categorie';
import { TypeVentePage } from '../pages/type-vente/type-vente';
import { VersementPage } from '../pages/versement/versement';
import { TypeTvPage } from '../pages/type-tv/type-tv';
import { CodebarreTvPage } from '../pages/codebarre-tv/codebarre-tv';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { TestPage } from '../pages/test/test';
import { Printer, PrintOptions } from '@ionic-native/printer';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AuthentifiactionPage,
    ProduitPage,VentePage,ProfilPage,ListeProduitPage,
    LubrifiantsPage,GazPage,
    ConsultationPage,CommandeProduitBlancPage,FirstPage,
    CommandeGazPage,SearchPipe,DetailProduitPage,
    LavagePage,LavageCategoriePage,DetailTransactionPage,GerantPage,ListeShiftPage,DetailShiftPage,AccessoirePage,
    StockProduitsPage,StockCategoriePage,TypeVentePage,
    VersementPage,TypeTvPage,CodebarreTvPage,TestPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule,HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AuthentifiactionPage,
    ProduitPage,VentePage,ProfilPage,ListeProduitPage,LubrifiantsPage,
    GazPage,ConsultationPage,CommandeProduitBlancPage,
    FirstPage,CommandeGazPage,DetailProduitPage,LavagePage,
    LavageCategoriePage,DetailTransactionPage,GerantPage,ListeShiftPage,DetailShiftPage,AccessoirePage,
    StockProduitsPage,StockCategoriePage,TypeVentePage,
    VersementPage,TypeTvPage,CodebarreTvPage,TestPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,BarcodeScanner,File,FileOpener,Printer
  ]
})
export class AppModule {}
